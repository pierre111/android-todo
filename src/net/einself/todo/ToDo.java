package net.einself.todo;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import com.niusounds.sqlite.SQLiteDAO;

import java.util.ArrayList;
import java.util.HashMap;

public class ToDo extends ListActivity
{

    private SQLiteDAO taskDAO;

    private AlertDialog addDialog;

    private View addView;


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        // set DAO
        taskDAO = SQLiteDAO.getInstance(this, Task.class);

        // load tasks
        setListAdapter(getAdapter());

        // Create add-dialog
        addView = getLayoutInflater().inflate(R.layout.add, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Task").setView(addView)
            .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    // get text
                    EditText et = (EditText) addView.findViewById(R.id.add_editText_name);

                    // create task
                    Task task = new Task();
                    task.setName(et.getText().toString());

                    // save task
                    taskDAO.insert(task);

                    // clear EditText
                    et.setText("");

                    // refresh ListView
                    setListAdapter(getAdapter());
                }
            })
            .setNegativeButton("Cancel", null);
        addDialog = builder.create();
    }


    private SimpleAdapter getAdapter() {
        // Save all tasks in ArrayList as HashMaps
        ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        for (Task task : taskDAO.get(Task.class)) {
            list.add(task.getAsMap());
        }

        // mapping for ListView
        String[] from = {
                "name",
                "_id",
        };
        int[] to = {
                R.id.list_textView_taskName,
                R.id.list_textView_taskId
        };

        return new SimpleAdapter(this, list, R.layout.list, from, to);
    }


    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        // get taskId
        TextView taskId = (TextView) v.findViewById(R.id.list_textView_taskId);

        // remove task
        taskDAO.delete(Task.class, "_id=?", taskId.getText().toString());

        // refresh ListView
        setListAdapter(getAdapter());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_add_task:
                addDialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
