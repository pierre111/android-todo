package net.einself.todo;

import com.niusounds.sqlite.PrimaryKey;
import com.niusounds.sqlite.Persistence;

import java.util.HashMap;

public class Task {

    @Persistence
    @PrimaryKey(autoIncrement = true)
    private long _id;

    @Persistence
    private String name;

    @Persistence
    private boolean finished = false;


    public long getId() {
        return _id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }


    public HashMap<String, Object> getAsMap() {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("_id", getId());
        map.put("name", getName());
        map.put("finished", getFinished());
        return map;
    }

}
